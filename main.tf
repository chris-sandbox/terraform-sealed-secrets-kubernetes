terraform {
  required_providers {
    kustomization = {
      source  = "kbst/kustomization"
      version = ">= 0.2"
    }
  }
  required_version = ">= 0.12"
}

provider "kustomization" {}

data "kustomization_build" "sealed-secrets" {
  path = "${abspath(path.module)}/manifests"
}

resource "kustomization_resource" "sealed-secrets" {
  for_each = data.kustomization_build.sealed-secrets.ids

  manifest = data.kustomization_build.sealed-secrets.manifests[each.value]
}
